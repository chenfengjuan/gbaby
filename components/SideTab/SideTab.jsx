import React from "react"
import {TabControl,Tab} from '../TabControl/TabControl.jsx'
import Content from '../Content/Content.jsx'
import HelperForm from '../HelperForm/HelperForm.jsx'
import GroupForm from '../GroupForm/GroupForm.jsx'
import Table from '../Table/Table.jsx'
// import KeyForm from '../KeyForm/KeyForm.jsx'
import InForm from '../InForm/InForm.jsx'
// import EnterForm from '../UpEnterForm/EnterForm.jsx'
import H5Enter from '../H5Enter/H5Enter.jsx'
var SideTab = React.createClass({
    getInitialState: function(){
        return {
            helper:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:true,
                url:this.props.data.url,
                userID: this.props.data.userID,
            },
            group:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID
            },
            h5enter:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            },
            atgroup:{
                totals:1,
                page:1,
                count:0,
                data:[],
                isRefresh:false,
                url:this.props.data.url,
                userID: this.props.data.userID,
                helperData: this.props.data.helperData
            }
        }
    },
    changePage: function (args) {
        this.setState(function () {
            this.state[args.name].page=args.page;
            this.state[args.name].isRefresh=!this.state[args.name].isRefresh;
            return this.state;
        });
    },
    getData: function (args) {
        this.setState(function () {
            this.state[args.parent].data=args.data.data;
            this.state[args.parent].totals=args.data.totals;
            this.state[args.parent].page=args.data.page;
            this.state[args.parent].count=args.data.count;
            return this.state;
        });
    },
    render:function(){
        return (
            <TabControl>
                    <Tab name="小助手管理">
                        <Content name="helper" type="小助手管理" data={this.state.helper}>
                            <HelperForm name="helper" data={this.state.helper} callback={{returnData:this.getData}}></HelperForm>
                            <Table name="helper" data={this.state.helper} ></Table>
                        </Content>
                    </Tab>
                    <Tab name="群管理">
                        <Content type="群管理" name="group" data={this.state.group} callback={{changePage:this.changePage}}>
                            <GroupForm name="group" data={this.state.group} callback={{returnData:this.getData}}></GroupForm>
                            <Table name="group" data={this.state.group} ></Table>
                        </Content>
                    </Tab>
                    <Tab name="入群管理">
                        <Content type="入群管理" name="h5enter" data={this.state.h5enter} callback={{changePage:this.changePage}}>
                            <H5Enter name="h5enter" data={this.state.h5enter} callback={{returnData:this.getData}}></H5Enter>
                            <Table name="h5enter" data={this.state.h5enter} ></Table>
                        </Content>
                    </Tab>
                    <Tab name="小助手所在群">
                        <Content type="小助手所在群" name="atgroup" data={this.state.atgroup} callback={{changePage:this.changePage}}>
                            <InForm name="keyword" data={this.state.atgroup} callback={{returnData:this.getData}}></InForm>
                            <Table name="atgroup" data={this.state.atgroup} ></Table>
                        </Content>
                    </Tab>
            </TabControl>
        );
    }
});

export default SideTab
