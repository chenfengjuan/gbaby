import './KeyForm.css'
import React from 'react'
import ReactSelect from '../ReactSelect/ReactSelect.jsx'
import Form from '../Form/Form.jsx'

class KeyForm extends Form{
    constructor(props){
        super(props);
        var selectData=[{name:"All",value:this.props.data.userID}];
        this.state={
            helperid:this.props.data.userID,
            key1:"",
            key2:"",
            edcTime:"",
            helperData:selectData.concat(this.props.data.helperData)
        }
        this._searchData=this.searchData.bind(this);
        this._requestData=this.requestData.bind(this);
        this._handleChange=this.handleChange.bind(this);
        this._requestData(1);
    }
    handleChange(e){
        this.setState({
            [e.target.name]:e.target.value
        });
        if(e.target.name=="helperid"){
            this.state[e.target.name]=e.target.value;
            this._requestData(1);
        }
    }
    componentDidMount(){
        var self=this;
        //配置时间控件
        $("#edcTime").jeDate({
            ishmsVal:false,
            format:"YYYY-MM-DD",
            zIndex:3000,
            choosefun: function (elem, val) {
                self.setState({
                    [$(elem).attr("name")]: val
                });
            },
            okfun: function (elem, val) {
                self.setState({
                    edcTime: val
                });
            },
            clearfun: function (elem, val) {
                self.setState({
                    [$(elem).attr("name")]: ""
                });
            }
        })
    }
    getFoemData(page){
        var formData=new Object();
        formData.robotID=this.state.helperid;
        formData.city=this.state.key1;
        formData.keyword=this.state.key2;
        formData.edc=this.state.edcTime;
        formData.page=page==undefined?1:page;
        formData.pageSize=20;
        if(this.state.helperid==this.props.data.userID){
            formData.type="all";
        }else{
            formData.type="";
        }
        return formData;
    }
    requestData(page){
        var self=this;
        $.ajax({
            type: 'POST',
            url: "http://wyeth.gemii.cc//HelperManage/keyword/selectKeyword",
            data: self.getFoemData(page),
            dataType: 'json',
            success: function(data){
                data.count=data.data.total;
                data.totals=data.data.count;
                data.page=page;
                data.data.tbody=self.getTbody(data.data.keywords);
                data.data.thead=self.setThead();
                self.props.callback.returnData({data:data,parent:"keyword"});
            }
        });
    }
    setThead(){
      var thead=[{name:"ID",width:'10%'},
          {name:"小助手名",width:'14%'},
          {name:"关键字1",width:'9%'},
          {name:"关键字2",width:'20%'},
          {name:"预产期范围",width:'18%'},
          {name:"关键字匹配群",width:'18%'}
      ];
      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    getTbody(data){
        var tr=data.map(function (item, i) {
            return <tr key={i}>
                <td>{item.roomid}</td>
                <td>{this.checkNull(item.robotName)}</td>
                <td>{this.checkNull(item.city)}</td>
                <td>{this.checkNull(item.keyword)}</td>
                <td>{this.checkNull(item.startTime)} ~ {this.checkNull(item.endTime)}</td>
                <td>{item.roomName}</td></tr>
        }.bind(this));
        return tr;
    }
    searchData(){
        this._requestData(1);
    }
    render(){
        return <div className="KeyForm-formCss">
          <ReactSelect placeholder="小助手" dataSearch="true" data={this.state.helperData}
                       className="KeyForm-helper" name="helperid"
                       onChange={this._handleChange} >
          </ReactSelect>
            <input placeholder="关键字1" name="key1" value={this.state.key1} className="KeyForm-inputCSS KeyForm-helpersCSS1" onChange={this._handleChange}></input>
            <input placeholder="关键字2" name="key2" value={this.state.key2} className="KeyForm-inputCSS KeyForm-helpersCSS1" onChange={this._handleChange}></input>
            <input placeholder="edc时间" name="edcTime" value={this.state.edcTime} id="edcTime" className="KeyForm-inputCSS KeyForm-helpersCSS1" onChange={this._handleChange}></input>
            <button className="KeyForm-buttonCSS" onClick={this._searchData}>搜索</button>
        </div>
    }
}

export default KeyForm
