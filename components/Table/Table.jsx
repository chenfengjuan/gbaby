import React from 'react'
import './Table.css'

class Table extends React.Component{
    constructor(props){
        super(props);
    }
    checkNull(arg){
        if(arg==null){
            return "";
        }else {
            return arg;
        }
    }
    getTableCSS(){
        return this.props.data.data.tableCSS==undefined?"Table-taskTable":"Table-taskTable "+this.props.data.data.tableCSS;
    }
    render(){
        return<table className={this.getTableCSS()}>
            <thead>
            <tr>
                {
                    this.props.data.data.thead
                }
            </tr>
            </thead>
            <tbody>
            {
                this.props.data.data.tbody
            }
            </tbody>
        </table>
    }
}

export default Table
