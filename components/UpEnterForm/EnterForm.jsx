import React from 'react'
import './EnterForm.css'
import ReactSelect from '../ReactSelect/ReactSelect.jsx'
import Form from '../Form/Form.jsx'

class UpEnterForm extends Form{
    constructor(props){
        super(props);
        this.state={
            helperid:"2326791867",
            msg:"",
            name:"",
            status:"",
            data:[]
        }
        this._searchData=this.searchData.bind(this);
        this._requestData=this.requestData.bind(this);
        this._changeEnter=this.changeEnter.bind(this);
        this._addData=this.addData.bind(this);
        this._addSave=this.addSave.bind(this);
        this._openOverflow=this.openOverflow.bind(this);
        this._cancelAdd=this.cancelAdd.bind(this);
        this._handleChange=this.handleChange.bind(this);
        this._requestData(1);
    }
    handleChange(e){
        this.setState({
            [e.target.name]:e.target.value
        });
        if(e.target.name=="status"||e.target.name=="helperid"){
            this.state[e.target.name]=e.target.value;
            this._requestData(1);
        }
    }
    cancelAdd(e){
      var index=$(e.target).prop("name");
      var del;
      $.each(this.state.data.data.tbody,function(i,item){
          if(item.key==index){
            del=i;
            return null;
          }
      });
      this.state.data.data.tbody.splice(del,1);
      var key=this.state.data.data.tbody[0].key;
      if(key==0){
        this.state.data.data.thead[7]=<th key={7} style={{width:'0'}}></th>;
      }
      this.props.callback.returnData({data:this.state.data,parent:"upenter"});
    }
    changeEnter(e){
        var self=this;
        var text=e.target.value;
        var data=new Object();
        data.messagestatus=e.target.value;
        data.id=e.target.name;
        if (confirm("确定要修改入群状态吗？")) {
            // 发送保存修改的请求
            $.ajax({
                type: "POST",
                url: "http://wyeth.gemii.cc/HelperManage/message/modifyMessage",
                data: data,
                dataType: "json",
                success: function (data) {
                    self._requestData(self.props.data.page);
                }
            });
        }else{
            if(text=="手动邀请"){
                $(e.target).val("未入群");
            }else{
                $(e.target).val("手动邀请");
            }
        }
    }
    requestGroup(robotID){
        var groupData=[];
        $.ajax({
            type: 'get',
            url: "http://wyeth.gemii.cc/HelperManage/group/getGroupByRobot?page=1&type=&pageSize=100&groupName=&robotID="+robotID,
            async: false,
            data: "",
            dataType: 'json',
            success: function(data){
                groupData=data.data.groups;
            }
        });
        return groupData
    }
    testKey(l){
      for(let item of this.state.data.data.tbody){
        if(item.key==l){
          l++;
        }
      }
      return l;
    }
    addData(){
        var groupData=this.requestGroup(this.state.helperid);
        var l=19-this.state.data.data.tbody.length;
        l=this.testKey(l);
        var addTr=[<tr key={l}>
          <td className='UpEnterForm-tdCSS'>
            <input type='text' className='UpEnterForm-inputCSS' placeholder='输入昵称'/>
          </td>
        <td className='UpEnterForm-tdCSS'>
          <input type='text' className='UpEnterForm-inputCSS UpEnterForm-width0' placeholder='上海+虹口妇幼+20161101'/>
        </td>
        <td className='UpEnterForm-tdCSS'>
          <input type='text' className='UpEnterForm-inputCSS UpEnterForm-width2' placeholder='输入提醒消息'/>
        </td>
        <td className='UpEnterForm-tdCSS'>
          <select className='UpEnterForm-selectCSS UpEnterForm-width1' name='groupNa'>
            {
              groupData.map(function(item,i){
                return <option key={i} value={item.roomID}>{item.roomName}</option>;
              })
            }
           </select>
        </td>
        <td className='UpEnterForm-tdCSS'>手动邀请</td>
        <td className='UpEnterForm-tdCSS'>
          <input type='text' className='UpEnterForm-inputCSS UpEnterForm-datainp'
            onClick={this._openDate}
            placeholder='年-月-日 时:分:秒'/>
        </td>
        <td className='UpEnterForm-tdCSS'>
          <select className='UpEnterForm-selectCSS UpEnterForm-width3'>
            <option>群匹配成功</option>
            <option>匹配全国群</option>
            <option>匹配城市群</option>
            <option>未匹配到群</option>
          </select>
        </td>
        <td>
          <a className='UpEnterForm-h5A UpEnterForm-add' name={l} onClick={this._addSave}>保存</a>
          <a className='UpEnterForm-h5A UpEnterForm-off' name={l} onClick={this._cancelAdd}>取消</a>
        </td>
      </tr>];
      this.state.data.data.tbody=addTr.concat(this.state.data.data.tbody);
      this.state.data.data.thead[7]=<th key={7} style={{width:'9%'}}>操作</th>;
      this.props.callback.returnData({data:this.state.data,parent:"upenter"});
    }
    addSave(e){
        var self=this;
        var addTr=$(e.target).parent().parent().children();
        if(addTr.eq(0).find("input").val()==""){
            alert("昵称不能为空");
        }else if(addTr.eq(1).find("input").val()==""){
            alert("消息内容不能为空");
        }else if(addTr.eq(1).find("input").val().split(/[+＋]/).length!=3){
            alert("消息内容格式错误");
        }else {
            if (confirm("保存后不能再删除，确定保存吗？")) {
                var td=$(e.target).parent();
                var data=new Object();
                data.username=td.parent().children().eq(0).find("input").val();
                data.upmessage=td.parent().children().eq(1).find("input").val();
                data.remindmessage=td.parent().children().eq(2).find("input").val();
                data.roomName=td.parent().children().eq(3).find("select option:selected").text();
                data.messagestatus=td.parent().children().eq(4).text();
                data.messagingtime=td.parent().children().eq(5).find("input").val();
                data.matchstatus=td.parent().children().eq(6).find("select").val();
                data.roomid=td.parent().children().eq(3).find("select").val();
                data.robotid=self.state.helperid;
                //发送保存修改的请求
                console.log(data);
                $.ajax({
                    type: "POST",
                    url: "http://wyeth.gemii.cc/HelperManage/message/insertMessage",
                    data: data,
                    dataType: "json",
                    success: function (data) {
                        self._cancelAdd(e);
                    }
                });
            }
        }
    }
    openOverflow(e){
        e.persist();
        var target;
        if($(e.target).prop("tagName")!="TR"){
            target=$(e.target).parent("tr");
        }else{
            target=$(e.target);
        }
        if(target.hasClass("UpEnterForm-textOverflow")){
            target.removeClass("UpEnterForm-textOverflow");
        }else{
            target.addClass("UpEnterForm-textOverflow");
        }
    }
    getFormData(page){
        var formData=new Object();
        formData.robotID=this.state.helperid;
        formData.upMessage=this.state.msg;
        formData.username=this.state.name;
        formData.status=this.state.status;
        formData.page=page==undefined?1:page;
        formData.pageSize=20;
        return formData;
    }
    requestData(page){
        var self=this;
        $.ajax({
            type: 'POST',
            url: 'http://wyeth.gemii.cc/HelperManage/message/selectMessage',
            data: self.getFormData(page),
            dataType: 'json',
            success: function(data){
                data.count=data.data.total;
                data.totals=data.data.count;
                data.page=page;
                data.data.tbody=self.getTbody(data.data.messages);
                data.data.thead=self.setThead();
                data.data.tableCSS=self.setTableCss();
                self.state.data=data;
                self.props.callback.returnData({data:data,parent:"upenter"});
            }
        });
    }
    setThead(){
      var thead=[{name:"昵称",width:'15%'},
          {name:"消息内容",width:'18%'},
          {name:"提醒消息",width:'13%'},
          {name:"微信群名",width:'20%'},
          {name:"入群状态",width:'10%'},
          {name:"消息时间",width:'16%'},
          {name:"群匹配状态",width:'11%'},
          {name:"",width:'0'}
      ];
      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    setTableCss(){
      return "UpEnterForm-tableLayoutUp";
    }
    getTbody(data){
        var tr=data.map(function (item, i) {
            var enter=this.checkNull(item.messagestatus);
            var tdCSS="";
            if((enter=="未入群")||(enter=="手动邀请")){
                tdCSS="UpEnterForm-tdCSS";
                var enter2="";
                if(enter=="未入群"){
                    enter2="手动邀请";
                }else{
                    enter2="未入群";
                }
                enter=<select name={item.id} className='UpEnterForm-selectCSS UpEnterForm-enterSelectCSS' defaultValue={enter} onChange={this._changeEnter}>
                    <option value={enter}>{enter}</option>
                    <option value={enter2}>{enter2}</option>
                </select>
            }

               return <tr key={i} onDoubleClick={this._openOverflow} className='UpEnterForm-textOverflow'>
                   <td>{this._emojiTo(this.checkNull(item.username))}</td>
                   <td>{this.checkNull(item.upmessage)}</td>
                   <td>{this.checkNull(item.remindmessage)}</td>
                   <td>{this.checkNull(item.roomName)}</td>
                   <td className={tdCSS}>{enter}</td>
                   <td>{this.checkNull(item.messagingtime).split(".")[0]}</td>
                   <td>{this.checkNull(item.matchstatus)}</td>
                   <td></td></tr>

        }.bind(this));
        return tr;
    }
    searchData(){
        this._requestData(1);
    }
    render(){
        return <div className="UpEnterForm-formCss">
          <ReactSelect placeholder="小助手" dataSearch="true" data={this.props.data.helperData}
                       className="UpEnterForm-helper" name="helperid"
                       onChange={this._handleChange} >
          </ReactSelect>
            <input placeholder="消息" name="msg" value={this.state.msg} className="UpEnterForm-inputCSS UpEnterForm-hpCSS1" onChange={this._handleChange}></input>
            <input placeholder="昵称" name="name" value={this.state.name} className="UpEnterForm-inputCSS UpEnterForm-hpCSS1" onChange={this._handleChange}></input>
            <select className="UpEnterForm-selectCSS" name="status" onChange={this._handleChange} value={this.state.status}>
                <option value="">All</option>
                <option value="未入群">未入群</option>
                <option value="手动邀请">手动邀请</option>
                <option value="已邀请">已邀请</option>
            </select>
            <button className="UpEnterForm-buttonCSS" onClick={this._searchData}>搜索</button>
            <button className="UpEnterForm-buttonCSS UpEnterForm-addBTn" onClick={this._addData}>添加</button>
        </div>
    }
}

export default UpEnterForm
