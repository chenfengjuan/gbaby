import './WebHead.css'
import React from "react"

class WebHead extends React.Component{
    constructor(props){
        super(props);
        this._out=this.out.bind(this);
    }
    goGemii(){
        // window.location.href="";
    }
    out(){
      $.ajax({
        type: 'get',
        url: this.props.data.outUrl,
        success: function(data){
          if(data.status==200){
            window.location.href="login.html";
          }
        }
      });
    }
    render(){
        return <div className="WebHead-headerContainer">
            <a className="WebHead-logo" href="javascript:void(0);" onClick={this.goGemii}>&nbsp;</a>
            <span className="WebHead-title" onClick={this.goGemii}>{this.props.name}</span>
            <a className="WebHead-out" href="javascript:void(0);" onClick={this._out}></a>
        </div>
    }
}
export default WebHead
