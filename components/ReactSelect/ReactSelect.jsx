import React from 'react'
import './ReactSelect.css'
class ReactSelect extends React.Component{
    constructor(props){
        super(props);
        var selectData=this.requestData();
        var defaultName="";
        for(let item of selectData){
            if(item.value==this.props.defaultValue){
                defaultName=item.name;
            }
        }
        if(selectData.length==0){
          this.state={
              data:[],
              name:"",
              value:"",
              open:false,
              searchStr:"",
              Static:[]
          };
        }else{
          this.state={
              data:selectData,
              name:this.props.defaultValue==undefined?selectData[0].name : defaultName,
              value:this.props.defaultValue==undefined?selectData[0].value : this.props.defaultValue,
              open:false,
              searchStr:"",
              Static:selectData
          };
        }
        this._toggleOpen=this.toggleOpen.bind(this);
        this._selectItem=this.selectItem.bind(this);
        this._searchItem=this.searchItem.bind(this);
        this._changeHandle=this.changeHandle.bind(this);
    }
    requestData(){
      //获取小助手下拉菜单内容
      var selectData=new Array();
      if(this.props.incloudData!=undefined){
        selectData=[{name:"All",value:"All"}];
      }else{
        selectData=[];
      }
      $.ajax({
          type: 'get',
          url: this.props.data.url,
          data:this.props.data.data,
          async: false,
          dataType: 'json',
          success: function(data){
            console.log(data.data);
              $.each(data.data,function (i, item) {
                  selectData.push({
                      value:item.robotID,
                      name:item.robotName
                  });
              });
          }
      });
      console.log(selectData);
      return selectData
    }
    componentDidMount(){
        this.selLayout();
        this.setEvent();
    }
    selLayout(){
        var selectW=$(".react-select").width();
        $(".react-select-search").css({width:(selectW-10)+'px'});
    }
    setEvent(){
        var self=this;
        $("body").on("click",function (e) {
            if($(e.target).hasClass("react-select-btn")||($(e.target).parent().hasClass("react-select-btn"))){
                return true;
            }else if($(e.target).hasClass("react-select-search")||($(e.target).parent().hasClass("react-select-search"))){
                return false;
            }else if(($(e.target).hasClass("react-select-ul")||($(e.target).parent().hasClass("react-select-ul")))&&(!$(e.target).hasClass("noResult"))){
                return true;
            }else{
                self.setState({
                    open: false,
                    data: self.state.data,
                    searchStr:""
                });
            }
        });
    }
    getSelectCSS(){
        return this.props.className==undefined?"react-select":"react-select "+this.props.className
    }
    changeHandle(e){
        this.setState({
            [e.target.name]:e.target.value
        });
    }
    selectItem(e){
        this.setState({
            name: $(e.target).attr("name"),
            value: $(e.target).attr("value"),
            open: false,
            data: this.state.Static,
            searchStr:""
        });
        console.log({target:{name:this.props.name,value:$(e.target).attr("value")}});
        this.props.onChange({target:{name:this.props.name,value:$(e.target).attr("value")}});
    }
    toggleOpen(e){
        if(this.state.open){
          this.setState({
              open:!this.state.open
          });
        }else{
          var data=this.requestData();
          this.setState({
              open:!this.state.open,
              data: data,
              searchStr:"",
              Static: data
          });
        }

    }
    searchItem(e){
        if(e.which==13&&this.state.data.length!=0){
            this._selectItem({target:{value:this.state.data[0].value,name:this.state.data[0].name}});
        }else{
            var value=e.target.value;
            var data=[];
            for(let item of this.state.Static){
                if(item.name.indexOf(value)>-1){
                    data.push(item);
                }
            }
            this.setState({
                data: data,
                searchStr:value
            });
        }
    }
    getSearchCSS(){
        return this.props.dataSearch?"react-select-search":"none-box";
    }
    getListCSS(){
        return this.state.open?"react-select-list":"react-select-list none-box";
    }
    stopProp(e){
        e.stopPropagation();
    }
    getResultCSS(){
        return (this.state.data.length==0&&this.state.searchStr!="")?"noResult": "noResult none-box";
    }
    render(){
        return <div className={this.getSelectCSS()} onClick={this.stopProp}>
            <button type="button" className="react-select-btn" onClick={this._toggleOpen}>
                <span value={this.state.value}>{this.state.name}</span><span className="react-select-caret"></span>
            </button>
            <div className={this.getListCSS()}>
                <div className={this.getSearchCSS()}>
                    <input name="searchStr" placeholder="search" onChange={this._changeHandle} value={this.state.searchStr} className="react-select-input" onKeyUp={this._searchItem}/>
                </div>
                <ul className="react-select-ul">
                    <li key={-1} className={this.getResultCSS()}>No results matched {this.state.searchStr}</li>
                    {
                        this.state.data.map(function (item, i) {
                          console.log(item.value);
                            if(this.state.value==item.value){
                                return <li key={i} onClick={this._selectItem} value={item.value} name={item.name} className="react-select-active">{item.name}</li>
                            }
                            if((i==0)&&(this.state.searchStr!="")){
                                return <li key={i} value={item.value} onClick={this._selectItem}  name={item.name} className="react-select-select">{item.name}</li>
                            }
                            return <li key={i} value={item.value} onClick={this._selectItem} name={item.name} >{item.name}</li>
                        }.bind(this))
                    }
                </ul>
            </div>
        </div>
    }
}

export default ReactSelect
