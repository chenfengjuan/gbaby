import React from 'react'
import './TabControl.css'
var TabControl = React.createClass({
    getInitialState: function(){
        return {currentIndex: 0,
            childIndex:-1,
            1:false,
            3:false,
        }
    },
    getTitleItemCssClasses: function(element,index){
        return index === this.state.currentIndex ? "TabControl-active" : "";
    },
    getTitleItemSpanCssClasses: function (element,index) {
        var s=this.state[index] ? "TabControl-openChild TabControl-openChildActive" : "TabControl-openChild";
        return s;
    },
    getContentItemCssClasses: function(index){
        return index === this.state.currentIndex ? "TabControl-tab-content-item TabControl-active" : "TabControl-tab-content-item";
    },
    getContentChildItemCssClasses: function(index){
        return index === this.state.childIndex ? "TabControl-tab-content-item TabControl-active" : "TabControl-tab-content-item";
    },
    getTitleChildItemCssClasses: function (parentIndex,index) {
        if(this.state.currentIndex==parentIndex){
            return index === this.state.childIndex ? "TabControl-active" : "";
        }else {
            return "";
        }

    },
    startRippleAnimate: function (e, target) {
        var offset = target.offset();
        var x = e.pageX;
        var y = e.pageY;
        var $ripple = $('<span class="ripple"></span>');
        target.find(".ripple").remove();
        $ripple.appendTo(target).css({
            left: x - offset.left - $ripple.width() / 2,
            top: y - offset.top - $ripple.height() / 2
        });
        setTimeout(function () {
            target.find(".ripple").remove();
        },2000);
    },
    itemClick: function (e) {
        e.stopPropagation();
        var target=$(e.target).prop("tagName")=="SPAN"?$(e.target).parent():$(e.target);
        if(target.prop("tagName")=="LI"){
            var index=target.index();
            this.startRippleAnimate(e, target);
            this.setState({currentIndex: index,childIndex:-1});
        }
    },
    itemChildClick: function (e) {
        e.stopPropagation();
        var target=$(e.target).prop("tagName")=="SPAN"?$(e.target).parent():$(e.target);
        if(target.prop("tagName")=="LI"){
            var index=target.index();
            var parentIndex=target.parent().parent().index();
            this.startRippleAnimate(e, target);
            this.setState({currentIndex:parentIndex,childIndex: index});
        }
    },
    itemParentClick: function (e) {
        var target=$(e.target).prop("tagName")=="SPAN"?$(e.target).parent():$(e.target);
        if(target.prop("tagName")=="LI") {
            var index = target.index();
            this.startRippleAnimate(e, target);
            this.setState({
                [index]: !this.state[index]
            });
        }
    },
    render: function(){
        var that = this;
        return (
            <div className="TabControl-height100">
                <nav className="TabControl-sideContainer" >
                    <ul>
                        {React.Children.map(this.props.children, function(element, index){
                            if(element.props.type=="parent"){
                                return (<li onClick={that.itemParentClick}
                                            className={that.getTitleItemCssClasses(element,index)}>
                                    <span>{element.props.name}</span><span className={that.getTitleItemSpanCssClasses(element,index)}>&nbsp;&nbsp;</span>
                                    <ul className={that.state[index]?"":"TabControl-close"}>
                                        {
                                            React.Children.map(element.props.children,function (e, i) {
                                                return (<li onClick={that.itemChildClick} className={that.getTitleChildItemCssClasses(index,i)}><span>{e.props.name}</span></li>)
                                            })
                                        }
                                    </ul>
                                </li>)
                            }else{
                                return (<li onClick={that.itemClick} className={that.getTitleItemCssClasses(element,index)}><span>{element.props.name}</span></li>)
                            }
                        })}
                    </ul>
                </nav>
                <div className="TabControl-contentContainer">
                    {React.Children.map(this.props.children, function(element, index){
                        if(element.props.type=="parent"){
                            return (<div className={that.getContentItemCssClasses(index)}>
                                {React.Children.map(element.props.children,function (e, i) {
                                    return (<div className={that.getContentChildItemCssClasses(i)}>{e}</div>)
                                })}
                            </div>)
                        }else {
                            return (<div className={that.getContentItemCssClasses(index)}>{element}</div>)
                        }
                    })}
                </div>
            </div>
        )
    }
});
var Tab = React.createClass({
    render: function(){
        return (this.props.children);
    }
});

export {TabControl,Tab}
