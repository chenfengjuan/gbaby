import React from 'react'
import ReactDOM from 'react-dom'
import Form from '../Form/Form.jsx'
import './HelperForm.css'
import WaitAnimation from '../WaitAnimation/WaitAnimation.jsx'

class HelperForm extends Form{
    constructor(props){
        super(props);
        this.state={
            helpName:"",
            isRefresh:this.props.data.isRefresh
        };
        this._searchData=this.searchData.bind(this);
        this._sortOnline=this.sortOnline.bind(this);
        this._requestData=this.requestData.bind(this);
        this._refreshData=this.refreshData.bind(this);
        this._addHelper=this.addHelper.bind(this);
        this._requestOnline=this.requestOnline.bind(this);
        this._deleteHelper=this.deleteHelper.bind(this);
    }
    deleteHelper(e){
        var self=this;
        var tag=$(e.target).parent().parent().find("td:nth-child(3)").text();
        if(confirm("确定删除吗")){
            $.ajax({
                type: "POST",
                url: this.props.data.url+"EnterGroup/noauth/assistant/delrobot",
                data:{
                  tag: tag
                },
                success: function (data) {
                    if(data.status==200){
                        //需要重新渲染列表
                        self._requestData();
                    }
                }
            });
        }
    }
    requestOnline(e){
        var addurl=this.props.data.url+"EnterGroup/noauth/assistant/robot";
        var addname="扫码后机器人需要花几秒种时间启动";
        var title=e.target.name+"扫码上线";
        $("#helper").prepend('<div class="HelperForm-taskDetails"></div>');
        ReactDOM.render(<WaitAnimation type={title} method="GET" data={{tag:e.target.title,userID:this.props.data.userID}} container={"HelperForm-taskDetails"} name={addname} parent={"小助手管理"} callback={this._requestData} url={addurl}/>,
        $(".HelperForm-taskDetails")[0]);
    }
    searchData(){
        if(this.props.name=="helper"){
            var robot=this.state.helpName;
            if(robot!=""){
                for (var i=0;i<$("#helper tbody tr").length;i++){
                    if($("#helper tbody tr").eq(i).children().eq(0).text().indexOf(robot)!=-1){
                        $("#helper tbody").prepend($("#helper tbody tr").eq(i));
                    }
                }
            }
        }
    }
    componentDidMount(){
        this._requestData();
        setInterval(this._requestData,30000);
    }
    requestData(){
        var self=this;
        $.ajax({
            type: 'get',
            url: self.props.data.url+"EnterGroup/noauth/assistant/robots",
            data: {
                "userID":this.props.data.userID,
                "robotName":""
            },
            dataType: 'json',
            success: function(data){
              if(data.status==200){
                data.totals=1;
                data.page=1;
                data.count=data.data.length;
                data.data.tbody=self.getTbody(data.data);
                data.data.thead=self.setThead();
                self.props.callback.returnData({data:data,parent:"helper"});
              }
            }
        });
    }
    setThead(){
      var thead=[{name:"小助手名",width:'19%'},
          {name:"小助手ID",width:'19%'},
          {name:"小助手标签",width:'19%'},
          {name:"在线状态",width:'19%'},
          {name:"H5短链接",width:'19%'},
          {name:"操作",width:'19%'}];

      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    getTbody(data){
        var tr=data.map(function (item, i) {
          var str = "";
          if (item.status == "offline") {
              str = "点击扫码上线";
          }
          return <tr key={i}>
                  <td>{item.robotName}</td>
                  <td>{item.robotID}</td>
                  <td>{item.robotTag}</td>
                  <td><span>{item.status}</span><span>&nbsp;&nbsp;</span><a className="HelperForm-h5A" title={item.robottag} name={item.robotname} onClick={this._requestOnline}>{str}</a></td>
                  <td>{this.checkNull(item.unifiedURL)}</td>
                  <td><a className="HelperForm-h5A" onClick={this._deleteHelper}>删除</a></td>
                  </tr>
        }.bind(this));
        return tr;
    }
    refreshData(){
        this._requestData();
    }
    addHelper(){
        var addurl=this.props.data.url+"EnterGroup/noauth/assistant/robot";
        var addname="请扫码并确认授权登陆";
        var title="添加小助手";
        $("#helper").prepend('<div class="HelperForm-taskDetails"></div>');
        ReactDOM.render(<WaitAnimation type={title} method="POST" data={{userID:this.props.data.userID}} container={"HelperForm-taskDetails"} parent={"小助手管理"} name={addname} url={addurl} callback={this._requestData} />,$(".HelperForm-taskDetails")[0]);
    }
    sortOnline(e){
        var status=e.target.value;
        for (var i=0;i<$("#helper tbody tr").length;i++){
            if($("#helper tbody tr").eq(i).children().eq(3).text().indexOf(status)!=-1){
                $("#helper tbody").prepend($("#helper tbody tr").eq(i));
            }
        }
    }
    render(){
        return <div className="HelperForm-formCss">
            <input placeholder="小助手名称" name="helpName" value={this.state.helpName} className="HelperForm-inputCSS HelperForm-helpersCSS1" onChange={this._handleChange}></input>
            <button className="HelperForm-buttonCSS" onClick={this._searchData}>搜索</button>
            <select className="HelperForm-selectCSS" name="isOn" onChange={this._sortOnline}>
                <option value="online">online</option>
                <option value="offline">offline</option>
            </select>
            <button className="HelperForm-buttonCSS" onClick={this._addHelper}>＋添加小助手</button>
            <button className="HelperForm-buttonCSS" onClick={this._refreshData}>刷新</button>
        </div>
    }
}

export default HelperForm
