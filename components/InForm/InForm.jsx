import React from 'react'
import './InForm.css'
import ReactSelect from '../ReactSelect/ReactSelect.jsx'
import Form from '../Form/Form.jsx'

class InForm extends Form{
    constructor(props){
        super(props);
        var selectData=[{name:"All",value:"All"}];
        this.state={
            helperid:"All",
            groupName:"",
            selectData:selectData,
            isRefresh:this.props.data.isRefresh
        }
        this._searchData=this.searchData.bind(this);
        this._requestData=this.requestData.bind(this);
        this._handleChange=this.handleChange.bind(this);
        this._requestData(1);
    }
    handleChange(e){
        if(e.target.name=="helperid"){
            this.state[e.target.name]=e.target.value;
            this._requestData(1);
        }else{
          this.setState({
              [e.target.name]:e.target.value
          });
        }
    }
    getFormData(page){
        var formData=new Object();
        formData.robotID=this.state.helperid;
        formData.groupName=this.state.groupName;
        formData.userID=this.props.data.userID;
        formData.page=page!=undefined?page:1;
        formData.pageSize=20;
        console.log(formData);
        return formData;
    }
    requestData(page){
        var self=this;
        page=page==undefined?1:page;
        $.ajax({
            type: 'get',
            url: self.props.data.url+"EnterGroup/noauth/admin/robotGroup",
            data: self.getFormData(page),
            dataType: 'json',
            success: function(data){
                data.count=data.data.totalCount;
                data.totals=data.data.totalPage;
                data.page=page;
                data.data.tbody=self.getTbody(data.data.infos);
                data.data.thead=self.setThead();
                self.props.callback.returnData({data:data,parent:"atgroup"});
            }
        });
    }
    setThead(){
      var thead=[{name:"小助手名称",width:'17%'},
          {name:"小助手状态",width:'13%'},
          {name:"群名",width:'25%'},
          {name:"群code",width:'18%'}
      ];
      var tr=thead.map(function (item, i) {
          return <th key={i} style={{width:item.width}}>{item.name}</th>
      });
      return tr;
    }
    getTbody(data){
        var tr=data.map(function (item, i) {
            return <tr key={i}>
                <td>{item.robotName}</td>
                <td>{item.status}</td>
                <td>{item.groupName}</td>
                <td>{this.checkNull(item.groupID)}</td>
            </tr>
        }.bind(this));
        return tr;
    }
    searchData(){
        this._requestData(1);
    }
    render(){
        return <div className="InForm-formCss">
          <ReactSelect placeholder="小助手" dataSearch="true" data={this.props.data.helperData}
                       className="InForm-helper" name="helperid" incloudData={this.state.selectData}
                       onChange={this._handleChange} >
          </ReactSelect>
            <input placeholder="群名" name="groupName" value={this.state.groupName} className="InForm-inputCSS InForm-helpersCSS2" onChange={this._handleChange}></input>
            <button className="InForm-buttonCSS" onClick={this._searchData}>搜索</button>
        </div>
    }
}

export default InForm
